# Imports required 
import socket
import threading
import datetime
import MsgReport_pb2
import time
from pandas import *
from requests import get
import netifaces as ni
from requests import get




class counterthread():

        def __init__(self, target2, clientExternalIP, clientInternalIP, clientPort, 
                serverAddressPort, bufferSize, numIterations, timeout, delay=0):
            self.count = 0 
            self.averageRoundtripTime = 0
            self.target2 = target2
            self.clientExternalIP = clientExternalIP
            self.clientInternalIP = clientInternalIP
            self.clientPort = clientPort
            self.serverAddressPort = serverAddressPort
            self.bufferSize = bufferSize
            self.numIterations = numIterations
            self.delay = delay
            self.timeout = timeout


        def Connect2Server(self):
            time.sleep(self.delay)
            # print("Starting thread with port: ", self.clientPort)

            for reqNumber in range(self.numIterations):

                # Bytes to be sent
                bytesToSend = returnByteMessage(self.target2, self.clientExternalIP, self.clientPort)
                #Create a socket instance - A datagram socket
                UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
                UDPClientSocket.bind((self.clientInternalIP, self.clientPort))
                
                # Time when the packet is sent
                packetSentTime = time.time()

                # Send message to server using created UDP socket
                UDPClientSocket.sendto(bytesToSend, self.serverAddressPort)

                try: #Receive message from the server
                    UDPClientSocket.settimeout(self.timeout)
                    msgFromServer = UDPClientSocket.recvfrom(self.bufferSize)
                    # print(msgFromServer)
                    UDPClientSocket.close()
                    self.count += 1 

                except socket.timeout: #Didn't receive message from the server # print("closing socket timeout")
                    UDPClientSocket.close()
                
                # Time when the packet is received (or over the timout time )
                packetRecievedTime = time.time()
                pakcetTraverlTime = packetRecievedTime - packetSentTime
                self.averageRoundtripTime += pakcetTraverlTime

            self.averageRoundtripTime = self.averageRoundtripTime / self.numIterations

            return


def returnByteMessage(target2, clientIP, clientPort):
    message = MsgReport_pb2.MsgReport()
    message.messageID = generateMessageID(clientIP, clientPort)
    message.checkSum = checksum()
    message.target.extend([target2])
    message.clientIP = clientIP
    message.clientPort = clientPort
    return message.SerializeToString()

def checksum():
    return 1234


def generateMessageID(IP, port):
    time = datetime.datetime.now()
    ID = str(time) + str(IP) + str(port)
    return ID.encode('utf8')


def printGraphMatrix(serverGoodPutList):
    print(DataFrame(serverGoodPutList))
    return


def returnInternalClientIP():
    # TODO: This is a bit tricky, one of these methods should work
    # This works with mac
    return "127.0.0.1"
    return socket.gethostbyname(socket.gethostname())

    # this works for ubuntu 
    listOfInterfaces = ni.interfaces()
    if len(listOfInterfaces) < 2:
        print("We currently only see one interface ... ")
        print(" Please connect to the internet.") 
        return None
    ni.ifaddresses(listOfInterfaces[1])
    clientIP  = ni.ifaddresses(listOfInterfaces[1])[ni.AF_INET][0]['addr']

    # Just find it yourself 
    # clientIP = "192.168.0.15"

    print("We found " + str(clientIP) + " to be the internal client IP." )
    input()
    return clientIP


def returnExternalClientIP():
    # This is a way of getting the external IP
    return "127.0.0.1"
    clientIP = get('https://api.ipify.org').text
    print("We found " + str(clientIP) + " to be the external client IP." )
    return clientIP






def runNetworkRead(packetsFileName, travelTimeFileName, numPackets, numThreads, timeout):

    #Sample UDP Client - Multi threaded
    numThreads = numThreads
    clientPortRange = [i for i in range(10000, 10000 + numThreads)]
    
    clientInternalIP = returnInternalClientIP()
    clientExternalIP = returnExternalClientIP()
    if clientInternalIP == None:
        return 

    bufferSize = 1024
    timeout = timeout
    numGoodReq = 0
    numIterations = numPackets
    
    # Reading the server addresses
    file1 = open('bonusServersList.txt', 'r')
    Lines = file1.readlines()
    serverList = [( socket.gethostbyname(x.split(":")[0]), int(x.split(":")[1]) ) for x in Lines]
    
    print("This list of servers are : ", serverList)
    input("Press any key (enter) to continue ... ")


    # print(serverList)
    numServers = len(serverList)

    # Creating the list of data lists
    serverGoodPutList = []
    serverTravelTimeList = []


    for server1Index in range(0,numServers):

        thisNodeTotalPackets = []
        thisNodeAverageTravelTime = []

        for server2Index in range(0, numServers):

            # Num of successfull requests
            print("Start test for server " + str(server1Index) + " bounce to " + str(server2Index))

            # Getting the server data
            server1 = serverList[server1Index]
            server2 = serverList[server2Index]

            server1IP = server1[0]
            server1Port = server1[1]

            server2IP = server2[0]
            server2Port = server2[1]

            serverAddressPort = server1

            target2 = MsgReport_pb2.Targets()
            target2.IP = server2IP
            target2.Port = server2Port

            # Server IP address and Port number
            serverAddressPort   = (server1IP, server1Port)


            # Connect2Server forms the thread - for each connection made to the server
            ThreadObject = []
            ThreadList = []

            # Create as many connections as defined by ThreadCount
            for threadIndexSend in range(0, numThreads):
                clientPort = clientPortRange[threadIndexSend]
                ThreadObject.append(counterthread(target2, clientExternalIP, clientInternalIP, clientPort, serverAddressPort, bufferSize, numIterations, timeout))
                ThreadInstance = threading.Thread(target=ThreadObject[threadIndexSend].Connect2Server)
                ThreadList.append(ThreadInstance)
                ThreadInstance.start()

            # Main thread to wait till all connection threads are complete
            for threadIndexRecieve in range(0, numThreads):
                # print("Thread number " + str(threadIndexRecieve) + " ended")
                ThreadList[threadIndexRecieve].join()

            numGoodReq = 0
            for threadIndexRecieve in range(0, numThreads):
                numGoodReq += ThreadObject[threadIndexRecieve].count
            thisNodeTotalPackets.append(numGoodReq)

            averageTripTime = 0
            for threadIndexRecieve in range(0, numThreads):
                averageTripTime += ThreadObject[threadIndexRecieve].averageRoundtripTime
            averageTripTime = averageTripTime / numThreads
            thisNodeAverageTravelTime.append(averageTripTime)

            print("Completed test for server " + str(server1Index) + " bounce to " + str(server2Index))
            print("The number of correct responses is " + str(numGoodReq))

        serverGoodPutList.append(thisNodeTotalPackets)
        serverTravelTimeList.append(thisNodeAverageTravelTime)

        printGraphMatrix(serverGoodPutList)
        printGraphMatrix(serverTravelTimeList)


    print("The final matrix for packets looks like bellow: ")
    print(printGraphMatrix(serverGoodPutList))

    print("The final matrix for trave time looks like bellow: ")
    print(printGraphMatrix(serverTravelTimeList))

    print("Saving the Data to files " + packetsFileName + " and " +  " ... ")
    DataFrame(serverGoodPutList).to_pickle(packetsFileName)
    DataFrame(serverTravelTimeList).to_pickle(travelTimeFileName)
    
    print("Experiment complete")