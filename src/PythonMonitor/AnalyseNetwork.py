from UDPClient import *
from pandas import *
import networkx as nx
import matplotlib.pyplot as plt
import random


numPackets = 100
numThreads = 1
numTotalReq = numPackets * numThreads
edge_width = 5
timeout = 0.5



def randomColor():
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))

def printGraphMatrix(serverGoodPutList):
    print(DataFrame(serverGoodPutList))
    return

packetsfileName = "packetsMatrix.pkl"
travelTimefileName = "travelTimeMatrix.pkl"
rerunTests = input("Would you like to rerun the test(y/n): ")
if rerunTests == "y":
    runNetworkRead(packetsfileName, travelTimefileName, numPackets, numThreads, timeout)
    print("Test Complete")

print("Moving on showing the graphs.")
print("---------------------------------------------------------------------------------------------------------------")


dfPackets = read_pickle(packetsfileName)
dfTravelTime = read_pickle(packetsfileName)


print("This is the packet matrix ... ")
print(printGraphMatrix(dfPackets))
input("Please press enter ... ")

print("This is the travel time matrix ... ")
print(printGraphMatrix(dfTravelTime))
input("Please press enter ... ")


numNodes = len(list(dfPackets))

stringInput = input("Press enter to continue, press q to quit: ")
while(stringInput != "quit"):
    
    stringInput = input("Input command (enter 0 for client), showAll --- showNodes:2,3 ... --- dontShowNodes:1,3 ... --- showEdges:(1,2),(2,3):")
    # print(stringInput)

    print("______________________________________________________________________________________________")
    if stringInput == "showAll":
    
        print("Showing all nodes: ")

        g = nx.DiGraph() 

        for x in range(0, numNodes + 1):
            g.add_node(str(x))

        for x in range(0,numNodes):
            g.add_edge( "0", str(x + 1), weight=sum([ dfPackets[y][x] for y in range(0, numNodes)]) / numNodes , color = 'grey'  )

        for y in range(0,numNodes):
            currentColor = randomColor()
            for x in range(0, numNodes):
                if x==y and dfPackets[x][y] != 0:
                    g.add_edge(str(y + 1), str(0), weight=dfPackets[x][y], color=currentColor)
                if dfPackets[x][y] != 0:
                    g.add_edge(str(y + 1), str(x + 1), weight=dfPackets[x][y], color=currentColor)



        maxSuccessfull = max([ d['weight'] for (u,v,d) in g.edges(data=True)])
        edgeLabels = nx.get_edge_attributes(g,'weight')
        edgewidth = [ edge_width * d['weight'] / (maxSuccessfull) for (u,v,d) in g.edges(data=True)]
        pos = nx.spring_layout(g, iterations=50)
        colors = [g[u][v]['color'] for u,v in g.edges()]
        nx.draw_circular(g, with_labels = True, edge_color=colors ,edge_labels=edgeLabels, width=edgewidth)
        plt.show()
    

    #---------------------------------------------------------------------------------------------------------------
    if stringInput.split(":")[0] == "showNodes":
        nodesToShow = stringInput.split(":")[1].split(",")
        nodesToShow = [int(x) for x in nodesToShow]
        print("Nodes to show: ", nodesToShow)

        numIncorrectNodes = len([x for x in nodesToShow if x > numNodes ])
        if numIncorrectNodes > 0:
            print("Incorrect nodes enterred: ")
            continue

        g = nx.DiGraph() 

        for x in range(0, numNodes + 1):
            g.add_node(str(x))

        for x in range(0,numNodes):
            if (x+1 in nodesToShow) and (0 in nodesToShow):
                g.add_edge( "0", str(x + 1), weight=sum([ dfPackets[y][x] for y in range(0, numNodes)]) / numNodes , color = 'grey'  )


        for y in range(0,numNodes):
            currentColor = randomColor()
            for x in range(0, numNodes):
                if x==y and dfPackets[x][y] != 0 and ( (0 in nodesToShow) and (y+1 in nodesToShow) ):
                    g.add_edge(str(y + 1), str(0), weight=dfPackets[x][y] , color=currentColor)
                if dfPackets[x][y] != 0 and ( (x+1 in nodesToShow) and (y+1 in nodesToShow) ):
                    g.add_edge(str(y + 1), str(x + 1), weight=dfPackets[x][y] , color=currentColor)



        maxSuccessfull = max([ d['weight'] for (u,v,d) in g.edges(data=True)])
        edgeLabels = nx.get_edge_attributes(g,'weight')
        edgewidth = [ edge_width * d['weight'] / (maxSuccessfull) for (u,v,d) in g.edges(data=True)]
        pos = nx.spring_layout(g, iterations=50)
        colors = [g[u][v]['color'] for u,v in g.edges()]
        nx.draw_circular(g, with_labels = True, edge_color=colors ,edge_labels=edgeLabels, width=edgewidth)
        plt.show()


    #---------------------------------------------------------------------------------------------------------------
    if stringInput.split(":")[0] == "dontShowNodes":
        nodesToShow = stringInput.split(":")[1].split(",")
        nodesToShow = [int(x) for x in nodesToShow]
        print("Nodes not to show: ", nodesToShow)

        numIncorrectNodes = len([x for x in nodesToShow if x > numNodes ])
        if numIncorrectNodes > 0:
            print("Incorrect nodes enterred: ")
            continue

        g = nx.DiGraph() 

        for x in range(0, numNodes + 1):
            g.add_node(str(x))

        for x in range(0,numNodes):
            if (x+1 not in nodesToShow) or (0 not in nodesToShow):
                g.add_edge( "0", str(x + 1), weight=sum([ dfPackets[y][x] for y in range(0, numNodes)]) / numNodes , color = 'grey'  )


        for y in range(0,numNodes):
            currentColor = randomColor()
            for x in range(0, numNodes):
                if x==y and dfPackets[x][y] != 0 and ( (0 not in nodesToShow) or (y+1 not in nodesToShow) ):
                    g.add_edge(str(y + 1), str(0), weight=dfPackets[x][y], color=currentColor)
                if dfPackets[x][y] != 0 and ( (x+1 not in nodesToShow) or (y+1 not in nodesToShow) ):
                    g.add_edge(str(y + 1), str(x + 1), weight=dfPackets[x][y], color=currentColor)


        maxSuccessfull = max([ d['weight'] for (u,v,d) in g.edges(data=True)])
        edgeLabels = nx.get_edge_attributes(g,'weight')
        edgewidth = [ edge_width * d['weight'] / (maxSuccessfull) for (u,v,d) in g.edges(data=True)]
        pos = nx.spring_layout(g, iterations=50)
        colors = [g[u][v]['color'] for u,v in g.edges()]
        nx.draw_circular(g, with_labels = True, edge_color=colors ,edge_labels=edgeLabels, width=edgewidth)
        plt.show()

    else:
        print("Some commands are not added yet")
        print("Please make another query with some of the available commands")


    print("---------------------------------------------------------------------------------------------------------------")
