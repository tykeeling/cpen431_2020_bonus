package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.net.InetAddress;

public class DatagramMsg {
    final InetAddress ip;
    final int port;
    final byte[] datagram;

    DatagramMsg(InetAddress ip, int port, byte[] datagram) {
        this.ip = ip;
        this.port = port;
        this.datagram = datagram;
    }
}
