package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Console {
    private static final int level = 0;
    private static boolean print = false;
    private static boolean log = false;

    private static Logger logger = Logger.getLogger("MyLog");
    private static FileHandler fh;

    static void setup(int port, boolean printin, boolean login) {
        print = printin;
        log = login;

        logger.setUseParentHandlers(false);

        if (login) {
            try {

                // This block configure the logger with handler and formatter
                fh = new FileHandler("Serverlog_" + Integer.toString(port) + ".log");
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();
                fh.setFormatter(formatter);

                // the following statement is used to log any messages
                logger.info("Server Log:");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static boolean isLevel(int levels) {
        return levels <= level;
    }

    static void log(String message) {
        if (print) {
            System.out.println(message);
        }

        if (log) {
            logger.info(message);
        }
    }
}
