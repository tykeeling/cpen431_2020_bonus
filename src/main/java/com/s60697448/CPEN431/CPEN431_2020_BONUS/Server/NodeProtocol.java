package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import ca.NetSysLab.ProtocolBuffers.MessageReport.MsgReport;
//import ca.NetSysLab.ProtocolBuffers.MessageReport.Report;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;

public class NodeProtocol implements ProtocolLayer {

    // There will be a cache here.
    private final ATMCache cache;
    private final CRC32 crc32 = new CRC32();


    public NodeProtocol() {
        this.cache = new ATMCache(1501); // TIMEOUT in ms.
    }

    /* Returns: byte[] packet to be sent, or byte[0] if no packet should be sent (dropping packet)
     */
    @Override
    public DatagramMsg call(DatagramMsg datagramMsg) throws UnknownHostException {
        Console.log("Received packet from: " +
                datagramMsg.ip.toString() + ":"+ Integer.toString(datagramMsg.port));

        // BEGIN: PARSE MSG ELSE DROP
        MsgReport msg;

        try {
            msg = MsgReport.parseFrom(datagramMsg.datagram);
        } catch (IOException e) {
            return new DatagramMsg(null, 0, new byte[0]);
        }

        ByteString id = msg.getMessageID();

        if (msg.getCheckSum() != getChecksum(id.toByteArray())) { // ByteBuffer.allocate().put().array();
            return new DatagramMsg(null, 0, new byte[0]);
        }

        InetAddress sendAddress;
        int port;


        Console.log(msg.toString());

        if (msg.getTargetCount() > 0) {
            sendAddress = InetAddress.getByName(msg.getTarget(0).getIP());
            port = msg.getTarget(0).getPort();
        } else {
            return new DatagramMsg(InetAddress.getByName(msg.getClientIP()), msg.getClientPort(), msg.toByteArray());
        }
        // END: PARSE MSG


        return new DatagramMsg(sendAddress, port, cache.get(id,msg).toByteArray());
//        MsgReport outMsg = applayer(msg); // Cacheless
//        return new DatagramMsg(sendAddress, port, outMsg.toByteArray());
    }

    long getChecksum(byte[] arr) {
        crc32.reset();
        crc32.update(arr, 0, arr.length);
//        return crc32.getValue();
        return 1234;
    }

    private MsgReport applayer(MsgReport inreport) {

        return  MsgReport.newBuilder()
                .setMessageID(inreport.getMessageID())
//                .addAllReport(reports)
                .addAllTarget(inreport.getTargetList().subList(1, Math.max(inreport.getTargetCount(),1)))
                .setCheckSum(getChecksum(inreport.getMessageID().toByteArray()))
                .setClientIP(inreport.getClientIP())
                .setClientPort(inreport.getClientPort())
                .build();
    }

//    // https://www.tutorialspoint.com/guava/guava_caching_utilities.htm
//    // Also responsible for 0x02 out of space and 0x03 system overload

    private class ATMCache {
        private final Cache<ByteString, MsgReport> cache; // messageID, Msg
        private final int maxSize = 10000; // 30_000 Should handle up to 17_000 req/s with 1.5s lifespan.

        ATMCache(long timeout) {
            cache = Caffeine.newBuilder().expireAfterWrite(timeout, TimeUnit.MILLISECONDS)
                    .maximumSize(maxSize)
                    .build();
        }

        // Move to weights at some point
        MsgReport get(ByteString messageID, MsgReport inMsg) {
            MsgReport resp = cache.getIfPresent(messageID);

            if (resp == null) {
                resp = applayer(inMsg);
                cache.put(messageID, resp);
                return resp;
            }
            return resp;
        }

        long cacheSize() {
            return cache.estimatedSize();
        }
    }
}


