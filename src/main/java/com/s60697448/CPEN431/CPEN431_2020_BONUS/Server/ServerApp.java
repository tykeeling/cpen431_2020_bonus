package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class ServerApp
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "High Throughput Server Test" );

        int port = Integer.parseInt(args[0]);
        int buffsize = 16 * 1024;

        if (args[1].equals("all")) {
            Console.setup(port, true, true);
        } else if (args[1].equals("print")) {
            Console.setup(port, true, false);
        } else if (args[1].equals("log")) {
            Console.setup(port, false, true);
        }

        NodeProtocol nodeProtocol = new NodeProtocol();
        TransportLayer udp_datagram = new UDP_Datagram(port, buffsize, nodeProtocol);
        new ServerThread(udp_datagram).start();

    }
}
