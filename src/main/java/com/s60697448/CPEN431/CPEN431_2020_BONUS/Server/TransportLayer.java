package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.io.IOException;

public interface TransportLayer {
    void receive() throws IOException;
}
