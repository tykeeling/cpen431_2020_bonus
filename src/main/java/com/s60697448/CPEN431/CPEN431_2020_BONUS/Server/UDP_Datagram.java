package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;

public class UDP_Datagram implements TransportLayer {

    private final byte inBuf[]; // global assignment to avoid re-allocation and GC problems.

    private ProtocolLayer protocol;
    private DatagramSocket socket;

    public UDP_Datagram(int port, int buffsize, ProtocolLayer protocol) throws IOException {
        socket = new DatagramSocket(port);
        this.protocol = protocol;
        this.inBuf = new byte[buffsize];
    }

    @Override
    public void receive() throws IOException {

        DatagramPacket packet = new DatagramPacket(inBuf, inBuf.length);

        socket.receive(packet);

        DatagramMsg sendmsg = protocol.call( new DatagramMsg(
                packet.getAddress(),
                packet.getPort(),
                Arrays.copyOfRange(inBuf, 0, packet.getLength())
        ));

        if (Console.isLevel(7)) {
            System.out.printf("outbound datagram ip:port:len: %s:%d:%d\n",
                    sendmsg.ip, sendmsg.port, sendmsg.datagram.length);
        }
        if (sendmsg.datagram.length > 0) {
                packet = new DatagramPacket(sendmsg.datagram, sendmsg.datagram.length,
                        sendmsg.ip, sendmsg.port);
                socket.send(packet);
            }
        }
}


