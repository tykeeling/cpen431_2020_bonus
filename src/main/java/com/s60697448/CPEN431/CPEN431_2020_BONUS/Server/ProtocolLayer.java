package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;


import java.net.UnknownHostException;

/*  Request-Reply Protocol Layer
 *  Responsible for generating messageID, checksums, and proper reply/timeout protocol.
 *  Specifics and transport layer depend on interface implementation
 */
public interface ProtocolLayer {
    DatagramMsg call(DatagramMsg Msg) throws UnknownHostException;
}
