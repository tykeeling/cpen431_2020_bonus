package com.s60697448.CPEN431.CPEN431_2020_BONUS.Server;

import java.io.IOException;

public class ServerThread extends Thread {

    TransportLayer layer;

    public ServerThread(TransportLayer transportLayer) {
        this.layer = transportLayer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                layer.receive();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
