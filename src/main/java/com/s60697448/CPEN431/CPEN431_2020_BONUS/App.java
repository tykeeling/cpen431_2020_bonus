package com.s60697448.CPEN431.CPEN431_2020_BONUS;

import java.io.IOException;
import java.util.Arrays;

import com.s60697448.CPEN431.CPEN431_2020_BONUS.Server.ServerApp;
import com.s60697448.CPEN431.CPEN431_2020_BONUS.Client.ClientApp;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        if (args[0].equals("server")) {
            ServerApp.main(Arrays.copyOfRange(args, 1,args.length));
        } else if (args[0].equals("client")) {
            ClientApp.main(Arrays.copyOfRange(args, 1,args.length));
        } else {
            System.out.println("Invalid use: arg 0 must be \"server\" or \"client\"");
        }

    }
}
