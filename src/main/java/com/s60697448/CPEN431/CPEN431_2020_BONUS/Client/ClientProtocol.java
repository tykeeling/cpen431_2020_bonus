//package com.s60697448.CPEN431.CPEN431_2020_BONUS.Client;
//
//import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
//import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
//import ca.NetSysLab.ProtocolBuffers.Message.Msg;
//import ca.NetSysLab.ProtocolBuffers.MessageReport;
//import com.google.protobuf.ByteString;
//import com.google.protobuf.InvalidProtocolBufferException;
//import com.s60697448.CPEN431.CPEN431_2020_A3.Share.ErrCode;
//
//import java.io.IOException;
//import java.io.StreamCorruptedException;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//import java.util.Random;
//import java.util.concurrent.TimeoutException;
//import java.util.zip.CRC32;
//
//public class ClientProtocol implements ClientProtocolLayer {
//
//    private ClientTransportLayer transportLayer;
//    private int MAX_PACKET_SIZE = 16 * 1024;
//    private int timeout = 100;
//    private int num_attempts = 4;
//    private int port;
//
//    private enum ErrCode {
//        NO_ERROR, INVALID_CHECKSUM, INVALID_MSG_ID, ERROR
//    }
//
//    public ClientProtocol(InetAddress address, int port) {
//        this.port = port;
//        transportLayer = new ClientDatagram(address, port, timeout, MAX_PACKET_SIZE);
//    }
//
//
//    // Must wrap/unwrap payload inside Message protocol
//    @Override
//    public MessageReport.MsgReport call(MessageReport.MsgReport payload) throws TimeoutException, UnknownHostException {
//
//        for (int i = 1; i <= num_attempts; i++) {
//            try {
//
//                error = ErrCode.INVALID_PARSE;
//                transportLayer.setTimeout(timeout*i);
//                byte[] buf = transportLayer.call(sendMsg.toByteArray());
//                int len = transportLayer.getLastPacketLen();
//
//                Msg recMsg = parseMsg(buf, len); // throws to StreamCorruptedException
//
//                error = verifyMsg(sendMsg, recMsg);
//
//                switch (error) {
//                    case NO_ERROR:
//                        byte[] getKey = recMsg.getPayload().toByteArray();
//                        return KVResponse.parseFrom(recMsg.getPayload());
//
//                    default:
//                        throw new StreamCorruptedException(
//                                String.format("Detected corrupt packet #%d after %d milliseconds", i, timeout));
//                }
//
//            } catch (StreamCorruptedException | InvalidProtocolBufferException e) {
//                System.out.print(e.getMessage());
//                System.out.println(" with error code: " + error);
//            } catch (IOException e) {
//                System.out.println(String.format("Timeout on packet #%d after %d milliseconds", i, timeout));
//            }
//        }
//
//        throw new TimeoutException("Unable to complete call.");
//    }
//
//    @Override
//    public void setAddress(InetAddress address) {
//        transportLayer.setAddress(address);
//    }
//
//    @Override
//    public void setPort(int port) {
//        transportLayer.setPort(port);
//    }
//
//    /* returns byte array according to:
//    * | - - - - | - - | - - | - - - - - - - - |      | - | == 1 byte
//    *  ip addr   port  rand  nanotime               ( LITTLE_ENDIAN )
//    */
//    public byte[] generateMessageID() throws UnknownHostException {
//        Random rand = new Random();
//        InetAddress localAddress = InetAddress.getLocalHost();
//
//        ByteBuffer payload = ByteBuffer.allocate(16).order(ByteOrder.LITTLE_ENDIAN);
//        payload.put(localAddress.getAddress());
//        payload.putShort((short) port);
//        payload.put((byte) rand.nextInt());
//        payload.put((byte) rand.nextInt());
//        payload.putLong(System.nanoTime());
//
//        return payload.array();
//    }
//
//    static public long getChecksum(byte[] arr1, byte[] arr2) {
//        byte[] combined = ByteBuffer.allocate(arr1.length+arr2.length)
//                .put(arr1).put(arr2).array();
//        CRC32 crc32 = new CRC32();
//        crc32.update(combined);
//        return crc32.getValue();
//    }
//
//    /* generates Msg with messageID and checksum from payload
//     */
////    public Msg generateFullMsg(KVRequest payload) throws UnknownHostException{
////        byte[] header = generateMessageID();
////        byte[] bytePayload = payload.toByteArray();
////        long checksum = getChecksum(header, bytePayload);
////
////        Msg sendMsg = Msg.newBuilder()
////                .setMessageID(ByteString.copyFrom(header))
////                .setPayload(ByteString.copyFrom(bytePayload))
////                .setCheckSum(checksum)
////                .build();
////
////        return sendMsg;
////    }
////
////    // Compares MSG ID's and also verifies the checksum (of suspectMsg only)
////    static public ErrCode verifyMsg(Msg sentMsg, Msg suspectMsg) {
////        if (!sentMsg.getMessageID().equals(suspectMsg.getMessageID())) { // watch out for Java object == vs .equals()
//////            printByteString(sentMsg.getMessageID(), 16);
//////            printByteString(suspectMsg.getMessageID(), 16);
////            return ErrCode.INVALID_MSG_ID;
////        }
////
////        byte[] susID = suspectMsg.getMessageID().toByteArray();
////        byte[] susPayload = suspectMsg.getPayload().toByteArray();
////        long computeChecksum = getChecksum(susID, susPayload);
////
////        if (suspectMsg.getCheckSum() != computeChecksum) { // primitive type
////            return ErrCode.INVALID_CHECKSUM;
////        }
////
////        return ErrCode.NO_ERROR;
////    }
////
////    static public Msg parseMsg(byte[] arr, int len) throws InvalidProtocolBufferException {
////        ByteString arry = ByteString.copyFrom(arr, 0, len);
////        return Msg.parseFrom(arry);
////    }
//
//}
