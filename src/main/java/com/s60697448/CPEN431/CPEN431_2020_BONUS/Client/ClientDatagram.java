package com.s60697448.CPEN431.CPEN431_2020_BONUS.Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientDatagram implements ClientTransportLayer {

    private int port;
    private InetAddress address;
    private int timeout;
    private int buffsize;
    private int packetLength;

    ClientDatagram(InetAddress address, int port, int timeout) {
        setAddress(address);
        setPort(port);
        setTimeout(timeout);
        buffsize = 16 * 1024;
    }

    ClientDatagram(InetAddress address, int port, int timeout, int buffsize) {
        setAddress(address);
        setPort(port);
        setTimeout(timeout);
        setBuffsize(buffsize);
    }

    @Override
    public byte[] call(byte[] buf) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        socket.setSoTimeout(timeout);

        DatagramPacket packet = new DatagramPacket(buf, buf.length,
                address, port);
        socket.send(packet);

        // receiving
        buf = new byte[buffsize];
        packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);

        packetLength = packet.getLength();

        return packet.getData();
    }

    @Override
    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public void setAddress(InetAddress address) {
        this.address = address;
    }

    @Override
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public void setBuffsize(int buffsize) {
        this.buffsize = buffsize;
    }

    @Override
    public int getLastPacketLen() {
        return packetLength;
    }
}
