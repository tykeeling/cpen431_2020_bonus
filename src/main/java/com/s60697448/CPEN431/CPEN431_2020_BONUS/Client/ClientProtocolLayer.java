package com.s60697448.CPEN431.CPEN431_2020_BONUS.Client;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;
import ca.NetSysLab.ProtocolBuffers.MessageReport;

/*  Request-Reply Protocol Layer
 *  Responsible for generating messageID, checksums, and proper reply/timeout protocol.
 *  Specifics and transport layer depend on interface implementation
 */
public interface ClientProtocolLayer {

    MessageReport.MsgReport call(MessageReport.MsgReport payload) throws TimeoutException, UnknownHostException;
    void setAddress(InetAddress address);
    void setPort(int port);

}
