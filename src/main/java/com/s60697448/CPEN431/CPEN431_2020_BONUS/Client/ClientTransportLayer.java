package com.s60697448.CPEN431.CPEN431_2020_BONUS.Client;
import java.io.IOException;
import java.net.InetAddress;

public interface ClientTransportLayer {

    byte[] call(byte[] buf) throws IOException;
    void setAddress(InetAddress address);
    void setPort(int port);
    void setTimeout(int timeout);
    void setBuffsize(int buffsize);
    int getLastPacketLen();

}
