#!/usr/bin/env bash

# http://mywiki.wooledge.org/BashFAQ/035#getopts

show_help() {
cat <<EOF
Usage: ${0##*/} [-h] [-i SSHKEY] [-f HOSTLISTFILE (required)] 
    [-P PROCS] [COMMAND (required)]
Run COMMAND on all of the servers in HOSTLISTFILE

    -f HOSTLISTFILE     Provide list of files
    -h                  display this help message
    -i SSHKEY           use SSHKEY as key
    -P PROCS            number of processes to use (default 1)
EOF
}

HOSTLISTFILE=0
SSHKEY=0
PROCS=1

OPTIND=1
while getopts f:hi:P: opt; do
    case $opt in
        h)
            show_help
            exit 0
            ;;
        f) 
            echo f
            HOSTLISTFILE=$OPTARG
            ;;
        i)
            echo i
            SSHKEY=$OPTARG
            ;;
        P)
            echo P
            PROCS=$OPTARG
            ;;
        *)
            show_help >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND-1))
COMMAND=$@

if [ "$HOSTLISTFILE" == "0" ]; then
    show_help >&2
    exit 1
fi

echo $HOSTLISTFILE
echo $SSHKEY
echo $COMMAND

SSHCOMMAND=ssh
if [ "$SSHKEY" != "0" ]; then
    SSHCOMMAND="${SSHCOMMAND} -i \"${SSHKEY}\""
fi

SSHCOMMAND="${SSHCOMMAND} -l ubc_cpen431_8"
HELPERFILE=.multissh.helper.sh

cat > ${HELPERFILE} <<EOF
echo ${SSHCOMMAND} \$2 \$1
${SSHCOMMAND} \$2 \$1
EOF

#COMMAND="\"${COMMAND}\""

#echo "cat ${HOSTFILENAME} | xargs -L 1 -P ${PROCS} bash ${HELPERFILE} \"${COMMAND}\""
cat ${HOSTLISTFILE} | xargs -L 1 -P ${PROCS} bash ${HELPERFILE} "${COMMAND}"
