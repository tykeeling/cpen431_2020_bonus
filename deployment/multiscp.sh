#!/usr/bin/env bash

# http://mywiki.wooledge.org/BashFAQ/035#getopts

show_help() {
cat <<EOF
Usage: ${0##*/} [-h] [-i SSHKEY] [-f HOSTLISTFILE (required)] 
    [-P PROCS] [LOCALFILE] [REMOTEFILE]
Run COMMAND on all of the servers in HOSTLISTFILE

    -f HOSTLISTFILE     Provide list of files
    -h                  display this help message
    -i SSHKEY           use SSHKEY as key
    -P PROCS            number of processes to use (default 1)
EOF
}

HOSTLISTFILE=0
SSHKEY=0
PROCS=1

OPTIND=1
while getopts f:hi:P: opt; do
    case $opt in
        h)
            show_help
            exit 0
            ;;
        f) 
            echo f
            HOSTLISTFILE=$OPTARG
            ;;
        i)
            echo i
            SSHKEY=$OPTARG
            ;;
        P)
            echo P
            PROCS=$OPTARG
            ;;
        *)
            show_help >&2
            exit 1
            ;;
    esac
done

shift $((OPTIND-1))
LOCALFILE=$1
REMOTEFILE=$2

if [ "$HOSTLISTFILE" == "0" ]; then
    show_help >&2
    exit 1
fi

echo $HOSTLISTFILE
echo $SSHKEY
echo $LOCALFILE $REMOTEFILE

SCPCOMMAND=scp
if [ "$SSHKEY" != "0" ]; then
    SCPCOMMAND="${SCPCOMMAND} -i \"${SSHKEY}\""
fi

HELPERFILE=.multiscp.helper.sh

echo $SCPCOMMAND

cat > ${HELPERFILE} <<EOF
echo ${SCPCOMMAND} \$1 ubc_cpen431_8@\$3:\$2
${SCPCOMMAND} \$1 ubc_cpen431_8@\$3:\$2
EOF
chmod +x ${HELPERFILE}

#COMMAND="\"${COMMAND}\""

#echo "cat ${HOSTFILENAME} | xargs -L 1 -P ${PROCS} bash ${HELPERFILE} \"${COMMAND}\""
cat ${HOSTLISTFILE} | xargs -L 1 -P ${PROCS} ./${HELPERFILE} "${LOCALFILE}" "${REMOTEFILE}"
